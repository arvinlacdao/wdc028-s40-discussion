## WDC028-36

### Code-along
1. In the register/index.js, add input fields for first name, last name, and mobile number. Include mobile number in the input validation via useEffect. Send a fetch request to the API's corresponding route for user registration. 

2. Create an errors.js file in the data folder to contain possible error messages to be rendered. 

3. Create an errors subdirectory in the pages folder and within it add a file named [id].js which signifies to Next that this page will be dynamically routed.

4. Use a similar fetch request in the login page to send a POST request to the API endpoint responsible for authentication.

5. Upon successful authentication, set the returned JWT in the local storage, retrieve user info, keep user id and isAdmin in the global user state, and redirect to /courses/create for now. 

6. Before we modify the course creation page, go to the app entry point at *_app.js* and modify how we store global user state after authentication.

7. Modify the course creation page to submit form input to the API endpoint responsible for creating a new course.

8. Modify the courses index page to pre-render the page using pre-fetched data.

### Other Notes

- Add a '.env.local' file at root project folder and input NEXT_PUBLIC_API_URL=http://localhost:4000/api to set the API URL.
 
### Activity
1. Have the students implement the update course integration.
2. Have the students implement the archive course integration.