import { Row, Col, Card } from 'react-bootstrap'

export default () => {
    return (
        <Row>
            <HighlightColumn title="Learn from Home" content="Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex."/>
            <HighlightColumn title="Study Now, Pay Later" content="Ex Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui."/>
            <HighlightColumn title="Just 4/6 Months" content="Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing."/>
        </Row>
    )
}

const HighlightColumn = (props) => {
    return (
        <Col xs={ 12 } lg={ 4 } className="mb-3">
            <Card className="h-100">
                <Card.Body>
                    <Card.Title>
                        <h5 className="text-center">{ props.title }</h5>
                    </Card.Title>
                    <Card.Text className="text-justify">
                        { props.content }
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    )
}