import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'

import { UserProvider } from '~/contexts/UserContext'
import AppHelper from '~/app-helper'
import Navbar from '~/components/Navbar'

import '~/styles/global.css'
import 'bootstrap/dist/css/bootstrap.min.css'

export default ({ Component, pageProps }) => {
    const [user, setUser] = useState({ id: null, isAdmin: null })

    useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then((userData) => {
            if (typeof userData._id !== 'undefined') {
                setUser({ id: userData._id, isAdmin: userData.isAdmin })
            } else {
                setUser({ id: null, role: null })   
            }
        })
    }, [user.id])

    const unsetUser = () => {
        localStorage.clear()
        setUser({ id: null, isAdmin: null })
    }

    return (
        <UserProvider value={ { user, setUser, unsetUser } }>
            <Navbar/>
            <Container>
                <Component { ...pageProps }/>
            </Container>
        </UserProvider>
    )
}