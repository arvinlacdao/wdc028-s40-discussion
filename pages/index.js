import Banner from '~/components/Banner'
import Highlights from '~/components/Highlights'
import View from '~/components/View'

export default () => {
    const data = {
        title: 'Zuitt Coding Bootcamp',
        content: 'Opportunities for everyone, everywhere',
        destination: '/courses',
        label: 'Enroll now!'
    }
  
    return (
        <View title={ 'Homepage' }>
            <Banner data={ data }/>
            <Highlights/>
        </View>
    )
}